import requests
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4, MP4Cover
from mutagen.id3 import ID3, APIC, error
import sys
response = requests.get('http://mediaplayer.cloversites.com/v2/players/3805f4e4-1553-45ea-be76-06878338ba98')
for media in response.json()['player']['media']:
    id = str(media['id'])

    audioExt = media['download_url'][-3:].lower()
    audioFilename = id + '.' + audioExt
    artExt = media['thumbnails']['key'][-3:].lower()
    artFilename = id + '.' + artExt

    print(id + '.' + audioExt)
    print(id + '.' + artExt)

    if audioExt == 'mp3':
        audio = MP3(audioFilename, ID3=ID3)

        # add ID3 tag if it doesn't exist
        try:
            audio.add_tags()
        except error:
            pass

        audio.tags.add(
            APIC(
                encoding=3, # 3 is for utf-8
                mime='image/' + artExt, # image/jpeg or image/png
                type=3, # 3 is for the cover image
                desc=u'Cover',
                data=open(artFilename, 'rb').read()
            )
        )

        audio.save()
    else:
        other = MP4(audioFilename)

        imageFormat = MP4Cover.FORMAT_JPEG if artExt == 'jpg' else MP4Cover.FORMAT_PNG
        other['covr'] = [MP4Cover(open(artFilename, 'rb').read(), imageFormat)]

        other.save()