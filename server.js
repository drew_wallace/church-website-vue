'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const sendSeekable = require('send-seekable');
const compression = require('compression');
const api = require('./api');

require('dotenv').config()

let app = express();

const dcc = {
    _id: 1,
    username: 'dcc',
    password: process.env.TEMPORARY_DIRECTORY_PASSWORD
};
const dccyouth = {
    _id: 2,
    username: 'dccyouth',
    password: process.env.TEMPORARY_TEACHERPORTAL_PASSWORD
};
const dccadmin = {
    _id: 3,
    username: 'dccadmin',
    password: process.env.TEMPORARY_ADMIN_PASSWORD
};

// Register a login strategy
passport.use(new LocalStrategy(
    function (username, password, done) {
        // This should check again db
        if (username === dcc.username && password === dcc.password) {
            return done(null, dcc);
        } else if (username === dccyouth.username && password === dccyouth.password) {
            return done(null, dccyouth)
        } else if (username === dccadmin.username && password === dccadmin.password) {
            return done(null, dccadmin)
        } else {
            done({message: 'Invalid password.'}, false);
        }
    }
));

// Required for storing user info into session
passport.serializeUser(function(user, done) {
    done(null, user._id);
});

// Required for retrieving user from session
passport.deserializeUser(function(id, done) {
    // The user should be queried against db using the id
    let user = dcc;
    if(id == 2) {
        user = dccyouth;
    } else if(id == 3) {
        user = dccadmin;
    }
    done(null, user);
});

app.use(require('cookie-parser')());
app.use(bodyParser.json({limit: '70mb'}));
app.use(require('express-session')({secret: 'mysteries of God', resave: false, saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(sendSeekable);
app.use(compression({filter: (req, res) => {
    if (req.headers['x-no-compression'] || req.headers['accept'] === 'text/event-stream') {
        // don't compress responses with this request header
        return false
    }

    // fallback to standard filter function
    return compression.filter(req, res)
}}));

const pathToApp = __dirname;
app.use(express.static(`${pathToApp}/dist`));

app.get('/', api.sendIndex);
app.get('/getinvolved', api.sendIndex);
app.get('/give', api.sendIndex);
app.get('/gifts', api.sendIndex);
app.get('/youth', api.sendIndex);
app.get('/about', api.sendIndex);
app.get('/events', api.sendIndex);
app.get('/teacherportal/curriculum/:curriculumId', api.sendIndex);
app.get('/teacherportal/teams', api.sendIndex);
app.get('/teacherportal', api.sendIndex);
app.get('/media', api.sendIndex);
app.get('/directory', api.sendIndex);
// app.get('/worshipsamples', api.sendIndex);
app.get('/podcastadmin', api.sendIndex);

app.get('/podcast/sermons.xml', api.sendPodcast);
app.get('/podcast/:id.jpg', api.getPodcastJpg);
app.get('/podcast/:id.png', api.getPodcastPng);
app.get('/podcast/:id.mp3', api.getPodcastMp3);
app.get('/api/media', api.getPodcastEpisodes);
app.post('/api/podcast', api.ensureLoggedIn.bind(this, '/podcastadmin'), api.setUuid, api.createPodcastEpisode);
app.put('/api/podcast/:guid', api.ensureLoggedIn.bind(this, '/podcastadmin'), api.setUuid, api.updatePodcastEpisode);
app.delete('/api/podcast/:guid', api.ensureLoggedIn.bind(this, '/podcastadmin'), api.removePodcastEpisode);

app.get('/api/login', api.getUser);
app.post('/api/login', api.authenticate);

app.post('/api/attachment', api.ensureLoggedIn.bind(this, '/teacherportal'), api.requestAttachmentDownload);
app.get('/api/curriculums', api.ensureLoggedIn.bind(this, '/teacherportal'), api.getCurriculumns);
app.post('/api/plans/attachment', api.ensureLoggedIn.bind(this, '/teacherportal'), api.requestPlanAttachmentDownload);
app.get('/api/plans', api.ensureLoggedIn.bind(this, '/teacherportal'), api.getServicePlans);
app.get('/api/teams', api.ensureLoggedIn.bind(this, '/teacherportal'), api.getTeams);

app.get('/api/people/:personId', api.ensureLoggedIn.bind(this, '/directory'), api.getPerson);
app.get('/api/people', api.ensureLoggedIn.bind(this, '/directory'), api.getPeople);
app.get('/api/householdleaders', api.ensureLoggedIn.bind(this, '/directory'), api.getHouseholdLeaders);
app.get('/api/households', api.ensureLoggedIn.bind(this, '/directory'), api.getHouseholds);

app.get('/api/blog', api.getBlog);

// app.get('/api/worshipsamples', api.getWorshipSamples);

app.get('/api/events/church', api.getChurchEvents);
app.get('/api/events/special', api.getSpecialEvents);
app.get('/api/events/youth', api.getYouthEvents);

app.get('/api/sse', api.ensureLoggedIn.bind(this, '/podcastadmin'), api.sendS3Updates);

// rp({
//     method: 'POST',
//     uri: `https://api.planningcenteronline.com/services/v2/media/2260534/attachments/67162003/open`,
//     json: true
// }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)
// .then(data => {
//     console.log(data);
//     const x = true;
// })

// rp({
//     uri: `http://dunwoodycommunitychurch.cloversites.com/podcast/3805f4e4-1553-45ea-be76-06878338ba98.xml`,
//     json: true
// }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)
// .then(data => {
//     fs.writeFile('podcast.xml.original', data);
//     const x = true;
// })

app.use(api.errorHandler);

app.listen(51111, () => {
    console.log('express server listening on: 51111');
});
