const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const passport = require('passport');
const rp = require('request-promise');
const fastXmlParser = require('fast-xml-parser');
const moment = require('moment');
const uuidv4 = require('uuid/v4');
const {toXML} = require('jstoxml');
const multer = require('multer');
const ID3Writer = require('browser-id3-writer');
const mp3Duration = require('mp3-duration');
const {DOMParser, XMLSerializer} = require('xmldom');
const streamToBuffer = require('stream-to-buffer')
const fileType = require('file-type');
const _ = require('lodash');
const s3 = require('s3');
const SSE = require('express-sse');
const Entities = require('html-entities').XmlEntities;
require('request').auth;

const he = new Entities();

const pathToApp = __dirname;

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'podcasts/');
    },
    filename: function (req, file, callback) {
        if (file.mimetype == 'application/octet-stream') {
            streamToBuffer(file.stream, function (err, buffer) {
                if (err) {
                    callback(err);
                } else {
                    const type = fileType(buffer);
                    callback(null, req.podcastGuid + '.' + type.ext);
                }
            });
        } else {
            const extention = file.originalname.substr(-3).toLowerCase().replace(/\.jpeg$/, '.jpg');
            if (file.fieldname == 'audio' && ['mp3'].indexOf(extention) != -1) {
                callback(null, req.podcastGuid + '.' + extention);
            } else if (file.fieldname == 'image' && ['jpg', 'png'].indexOf(extention) != -1) {
                callback(null, req.podcastGuid + '.' + extention);
            } else {
                callback(new Error(`File type ${extention} not supported for ${file.fieldname}. Need ${file.fieldname == 'audio' ? 'mp3' : 'jpg or png'}.`));
            }
        }
    }
})
const upload = multer({storage});

const client = s3.createClient({
    maxAsyncS3: 20,     // this is the default
    s3RetryCount: 3,    // this is the default
    s3RetryDelay: 1000, // this is the default
    multipartUploadThreshold: 20971520, // this is the default (20 MB)
    multipartUploadSize: 15728640, // this is the default (15 MB)
    s3Options: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        // any other options are passed to new AWS.S3()
        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
    },
});

const sse = new SSE();

exports.sendIndex = function(req, res) {
    res.sendFile(`${pathToApp}/dist/index.html`);
}

exports.sendPodcast = async function(req, res) {
    res.sendFile(`${pathToApp}/podcast.xml`);
}

exports.getPodcastJpg = async function(req, res) {
    try {
        if (fs.existsSync(`podcasts/${req.params.id}.jpg`)) {
            res.setHeader('content-type', 'image/png');
            fs.createReadStream(`podcasts/${req.params.id}.jpg`).pipe(res);
        } else {
            throw 'File not found';
        }
    } catch (err) {
        res.status(404).send();
        console.error(err);
    }
}

exports.getPodcastPng = async function(req, res) {
    try {
        if (fs.existsSync(`podcasts/${req.params.id}.png`)) {
            res.setHeader('content-type', 'image/png');
            fs.createReadStream(`podcasts/${req.params.id}.png`).pipe(res);
        } else {
            throw 'File not found';
        }
    } catch (err) {
        res.status(404).send();
        console.error(err);
    }
}

exports.getPodcastMp3 = async function (req, res) {
    try {
        if (fs.existsSync(`podcasts/${req.params.id}.mp3`)) {
            let file = await fs.readFileAsync(`podcasts/${req.params.id}.mp3`);
            res.sendSeekable(file, {type: 'audio/mp3'});
        } else {
            throw 'File not found';
        }
    } catch (err) {
        res.status(404).send();
        console.error(err);
    }
}

const parsePodcastXml = async (req, res) => {
    try {
        const data = await fs.readFileAsync('podcast.xml', 'utf8');
        var options = {
            attributeNamePrefix: '',
            attrNodeName: false, //default is 'false'
            textNodeName: '#text',
            ignoreAttributes: false,
            ignoreNameSpace: false,
            allowBooleanAttributes: false,
            parseNodeValue: true,
            parseAttributeValue: true,
            trimValues: true,
            cdataTagName: '__cdata', //default is 'false'
            cdataPositionChar: '\\c',
            attrValueProcessor: a => he.decode(a, {isAttributeValue: true}),//default is a=>a
            tagValueProcessor: a => he.decode(a) //default is a=>a
        };

        return fastXmlParser.parse(data, options);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getPodcastEpisodes = async function (req, res) {
    try {
        const jsonObj = await parsePodcastXml(req, res);
        res.json(jsonObj);
    } catch (err) {
        console.error(err);
    }
}

function formatDuration(duration) {
    const durationObj = moment.duration(duration, 'seconds');
    let hours = durationObj.get('hours');
    let minutes = durationObj.get('minutes');
    let seconds = durationObj.get('seconds');

    if (hours < 10) {
        hours = `0${hours}`;
    }
    if (minutes < 10) {
        minutes = `0${minutes}`;
    }
    if (seconds < 10) {
        seconds = `0${seconds}`;
    }

    return `${hours}:${minutes}:${seconds}`;
}

exports.createPodcastEpisode = function (req, res) {
    upload.fields([{name: 'title'}, {name: 'author'}, {name: 'summary'}, {name: 'image'}, {name: 'audio'}, {name: 'previousImage'}])(req, res, async (err) => {
        if (err) {
            console.error(err);
            res.status(415).json({message: err.message});
            return;
        }

        try {
            let audio;
            let image;
            let imageFilename;
            let duration;

            if (req.files.image) {
                imageFilename = req.files.image[0].filename;
                image = await fs.readFileAsync(req.files.image[0].path);

                const params = {
                    localFile: `podcasts/${imageFilename}`,

                    s3Params: {
                        Bucket: 'dccpodcasts',
                        Key: `${req.podcastGuid}.${imageFilename.substr(-3)}`,
                        // other options supported by putObject, except Body and ContentLength.
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                    },
                };
                await new Promise((resolve, reject) => {
                    const uploader = client.uploadFile(params);
                    uploader.on('error', function(err) {
                        console.error('unable to upload:', err.stack);
                        reject(err);
                    });
                    uploader.on('progress', function() {
                        sse.send({action: 'Uploading', file: `${req.podcastGuid}.${imageFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                    });
                    uploader.on('end', async function() {
                        sse.send({action: 'Uploading', file: `${req.podcastGuid}.${imageFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                        await fs.unlinkAsync(`podcasts/${imageFilename}`);
                        resolve()
                    });
                });

                imageFilename = `${req.podcastGuid}.${imageFilename.substr(-3)}`;
            } else if (req.body.previousImage) {
                // image = await fs.readFileAsync(`podcasts/${req.body.previousImage}`);
                imageFilename = req.body.previousImage;
                image = await rp({
                    method: 'GET',
                    uri: `https://s3.amazonaws.com/dccpodcasts/${imageFilename}`,
                    encoding: 'binary'
                });
                image = Buffer.from(image, 'binary');
            }

            if (!req.files.audio) {
                throw new Error('Missing audio file');
            }

            audio = await fs.readFileAsync(req.files.audio[0].path);
            let audioFilename = req.files.audio[0].filename;

            if (image) {
                const writer = new ID3Writer(audio);
                writer.setFrame('APIC', {
                    type: 3,
                    data: image,
                    description: 'Cover'
                });
                writer.addTag();
                const taggedSongBuffer = Buffer.from(writer.arrayBuffer);
                await fs.writeFileAsync(req.files.audio[0].path, taggedSongBuffer);
            }

            duration = await new Promise((resolve, reject) => {
                mp3Duration(req.files.audio[0].path, (err, duration) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(formatDuration(duration));
                });
            });

            const params = {
                localFile: `podcasts/${audioFilename}`,

                s3Params: {
                    Bucket: 'dccpodcasts',
                    Key: `${req.podcastGuid}.${audioFilename.substr(-3)}`,
                    // other options supported by putObject, except Body and ContentLength.
                    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                },
            };
            await new Promise((resolve, reject) => {
                const uploader = client.uploadFile(params);
                uploader.on('error', function(err) {
                    console.error('unable to upload:', err.stack);
                    reject(err);
                });
                uploader.on('progress', function() {
                    sse.send({action: 'Uploading', file: `${req.podcastGuid}.${audioFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                });
                uploader.on('end', async function() {
                    sse.send({action: 'Uploading', file: `${req.podcastGuid}.${audioFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                    await fs.unlinkAsync(`podcasts/${audioFilename}`);
                    resolve()
                });
            });

            const xmlOptions = {
                header: false,
                indent: '      '
            };
            const xml = toXML({
                item: {
                    title: req.body.title || '',
                    'itunes:author': req.body.author || '',
                    'itunes:summary': req.body.summary || '',
                    'itunes:subtitle': req.body.summary || '',
                    'itunes:image': {
                        _attrs: {
                            href: imageFilename ? `https://s3.amazonaws.com/dccpodcasts/${imageFilename}` : ''
                        }
                    },
                    enclosure: {
                        _attrs: {
                            length: req.files.audio[0].size,
                            type: 'audio/mp3',
                            url: `https://s3.amazonaws.com/dccpodcasts/${req.podcastGuid}.${audioFilename.substr(-3)}`
                        }
                    },
                    guid: `http://dunwoodychurch.org/media?id=${req.podcastGuid}`,
                    pubDate: req.body.pubDate && moment(req.body.pubDate).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ') || '',
                    'itunes:duration': duration,
                }
            }, xmlOptions);
            const file = await fs.readFileAsync('podcast.xml', 'utf8');
            const xmlDoc = new DOMParser().parseFromString(file, 'text/xml');
            const latestEpisode = xmlDoc.getElementsByTagName('item')[0];
            const newEpisodeElement = new DOMParser().parseFromString(xml, 'text/xml');
            const insertedEpisode = latestEpisode.parentNode.insertBefore(newEpisodeElement.documentElement, latestEpisode);

            insertedEpisode.parentNode.insertBefore(xmlDoc.createTextNode('\n    '), insertedEpisode.nextSibling);
            insertedEpisode.appendChild(xmlDoc.createTextNode('    '))

            var XMLS = new XMLSerializer();
            var inp_xmls = XMLS.serializeToString(xmlDoc)
                .replace('itunes:author xmlns:itunes=""', 'itunes:author')
                .replace('itunes:author xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"', 'itunes:author');

            await fs.writeFileAsync('podcast.xml', inp_xmls);
            res.json({message: `Podcast added: http://dunwoodychurch.org/media?id=${req.podcastGuid}`, guid: `http://dunwoodychurch.org/media?id=${req.podcastGuid}`});
        } catch (err) {
            console.error(err);
            res.status(400).json({message: err.message});
        }
    });
}

exports.updatePodcastEpisode = function(req, res) {
    upload.fields([{name: 'guid'}, {name: 'title'}, {name: 'author'}, {name: 'summary'}, {name: 'image'}, {name: 'audio'}])(req, res, async (err) => {
        if (err) {
            console.error(err);
            res.status(415).json({message: err.message});
            return;
        }

        try {
            let duration;
            let audio;
            let audioFilename;
            let audioSize;
            let image;
            let imageFilename;

            const file = await fs.readFileAsync('podcast.xml', 'utf8');
            const xmlDoc = new DOMParser().parseFromString(file, 'text/xml');

            let episodes = Array.from(xmlDoc.getElementsByTagName('item'));
            let selectedEpisode = episodes.find(episode => episode.getElementsByTagName('guid')[0].textContent.replace('http://dunwoodychurch.org/media?id=', '') == req.podcastGuid)
            let imageUrl = selectedEpisode.getElementsByTagName('itunes:image')[0].getAttribute('href');

            if (req.files.audio || req.files.image || req.body.previousImage) {
                if (!req.files.image && !req.body.previousImage) {
                    if (imageUrl) {
                        imageFilename = imageUrl.replace('https://s3.amazonaws.com/dccpodcasts/', '');
                        image = await rp({
                            method: 'GET',
                            uri: `https://s3.amazonaws.com/dccpodcasts/${imageFilename}`,
                            encoding: 'binary'
                        });
                        image = Buffer.from(image, 'binary');
                    }
                } else {
                    if (req.body.previousImage) {
                        imageFilename = req.body.previousImage;
                        image = await rp({
                            method: 'GET',
                            uri: `https://s3.amazonaws.com/dccpodcasts/${imageFilename}`,
                            encoding: 'binary'
                        });
                        image = Buffer.from(image, 'binary');
                    } else {
                        image = await fs.readFileAsync(req.files.image[0].path);
                        imageFilename = req.files.image[0].filename;

                        const params = {
                            localFile: `podcasts/${imageFilename}`,

                            s3Params: {
                                Bucket: 'dccpodcasts',
                                Key: `${req.podcastGuid}.${imageFilename.substr(-3)}`,
                                // other options supported by putObject, except Body and ContentLength.
                                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                            },
                        };
                        await new Promise((resolve, reject) => {
                            const uploader = client.uploadFile(params);
                            uploader.on('error', function(err) {
                                console.error('unable to upload:', err.stack);
                                reject(err);
                            });
                            uploader.on('progress', function() {
                                sse.send({action: 'Uploading', file: `${req.podcastGuid}.${imageFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                            });
                            uploader.on('end', async function() {
                                sse.send({action: 'Uploading', file: `${req.podcastGuid}.${imageFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                                await fs.unlinkAsync(`podcasts/${imageFilename}`);
                                resolve()
                            });
                        });
                    }

                    const jsonObj = await parsePodcastXml(req, res);
                    const previousImages = _.groupBy(jsonObj.rss.channel.item.filter(media => media['itunes:image'].href), media => media['itunes:image'].href.replace(/^.*[\\/]/, ''));
                    const originalImageFilename = imageUrl.replace(/^.*[\\/]/, '');
                    if (imageUrl.substr(-3) === 'png' && imageFilename.substr(-3) == 'jpg' && previousImages[originalImageFilename].length <= 1) {
                        const params = {
                            Bucket: 'dccpodcasts',
                            Delete: {
                                Objects: [
                                    {Key: `${req.podcastGuid}.png`}
                                ]
                            }
                            // other options supported by deleteObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObject-property
                        };
                        await new Promise((resolve, reject) => {
                            const uploader = client.deleteObjects(params);
                            uploader.on('error', function(err) {
                                console.error('unable to delete:', err.stack);
                                reject(err);
                            });
                            uploader.on('progress', function() {
                                sse.send({action: 'Deleting', file: `${req.podcastGuid}.png`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                            });
                            uploader.on('end', async function() {
                                sse.send({action: 'Deleting', file: `${req.podcastGuid}.png`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                                resolve()
                            });
                        });
                    } else if (imageUrl.substr(-3) === 'jpg' && imageFilename.substr(-3) == 'png' && previousImages[originalImageFilename].length <= 1) {
                        const params = {
                            Bucket: 'dccpodcasts',
                            Delete: {
                                Objects: [
                                    {Key: `${req.podcastGuid}.jpg`}
                                ]
                            }
                            // other options supported by deleteObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObject-property
                        };
                        await new Promise((resolve, reject) => {
                            const uploader = client.deleteObjects(params);
                            uploader.on('error', function(err) {
                                console.error('unable to delete:', err.stack);
                                reject(err);
                            });
                            uploader.on('progress', function() {
                                sse.send({action: 'Deleting', file: `${req.podcastGuid}.jpg`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                            });
                            uploader.on('end', async function() {
                                sse.send({action: 'Deleting', file: `${req.podcastGuid}.jpg`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                                resolve()
                            });
                        });
                    }
                }

                if (req.files.image) {
                    imageFilename = `${req.podcastGuid}.${imageFilename.substr(-3)}`;
                }

                if (!req.files.audio) {
                    audioFilename = `${req.podcastGuid}.mp3`;
                    // audio = await fs.readFileAsync(`podcasts/${req.podcastGuid}.mp3`);
                    audio = await rp({
                        method: 'GET',
                        uri: `https://s3.amazonaws.com/dccpodcasts/${audioFilename}`,
                        encoding: 'binary'
                    });
                    audio = Buffer.from(image, 'binary');
                    audioSize = selectedEpisode.getElementsByTagName('enclosure')[0].getAttribute('length');
                    duration = selectedEpisode.getElementsByTagName('itunes:duration')[0].textContent;
                } else {
                    audio = await fs.readFileAsync(req.files.audio[0].path);
                    audioFilename = req.files.audio[0].filename;
                    audioSize = req.files.audio[0].size;

                    duration = await new Promise((resolve, reject) => {
                        mp3Duration(req.files.audio[0].path, (err, duration) => {
                            if (err) reject(err);
                            resolve(formatDuration(duration));
                        });
                    });
                }

                if (image) {
                    const writer = new ID3Writer(audio);
                    writer.setFrame('APIC', {
                        type: 3,
                        data: image,
                        description: 'Cover'
                    });
                    writer.addTag();
                    const taggedSongBuffer = Buffer.from(writer.arrayBuffer);
                    await fs.writeFileAsync(`podcasts/${audioFilename}`, taggedSongBuffer);
                }

                if (req.files.audio) {
                    const params = {
                        localFile: `podcasts/${audioFilename}`,

                        s3Params: {
                            Bucket: 'dccpodcasts',
                            Key: `${req.podcastGuid}.${audioFilename.substr(-3)}`,
                            // other options supported by putObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                        },
                    };
                    await new Promise((resolve, reject) => {
                        const uploader = client.uploadFile(params);
                        uploader.on('error', function(err) {
                            console.error('unable to upload:', err.stack);
                            reject(err);
                        });
                        uploader.on('progress', function() {
                            sse.send({action: 'Uploading', file: `${req.podcastGuid}.${audioFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                        });
                        uploader.on('end', async function() {
                            sse.send({action: 'Uploading', file: `${req.podcastGuid}.${audioFilename.substr(-3)}`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                            await fs.unlinkAsync(`podcasts/${audioFilename}`);
                            resolve()
                        });
                    });

                    audioFilename = `${req.podcastGuid}.${audioFilename.substr(-3)}`;
                }
            }

            selectedEpisode.getElementsByTagName('title')[0].textContent = req.body.title || '';
            selectedEpisode.getElementsByTagName('itunes:author')[0].textContent = req.body.author || '';
            selectedEpisode.getElementsByTagName('itunes:summary')[0].textContent = req.body.summary || '';
            selectedEpisode.getElementsByTagName('itunes:subtitle')[0].textContent = req.body.summary || '';
            if (imageFilename) {
                selectedEpisode.getElementsByTagName('itunes:image')[0].setAttribute('href', `https://s3.amazonaws.com/dccpodcasts/${imageFilename}`);
            }
            if (audio) {
                selectedEpisode.getElementsByTagName('enclosure')[0].setAttribute('length', audioSize);
                selectedEpisode.getElementsByTagName('enclosure')[0].setAttribute('url', `https://s3.amazonaws.com/dccpodcasts/${audioFilename}`);
                selectedEpisode.getElementsByTagName('itunes:duration')[0].textContent = duration;
            }
            selectedEpisode.getElementsByTagName('pubDate')[0].textContent = req.body.pubDate && moment(req.body.pubDate).locale('en').format('ddd, DD MMM YYYY HH:mm:ss ZZ') || '';

            var XMLS = new XMLSerializer();
            var inp_xmls = XMLS.serializeToString(xmlDoc)
                .replace('itunes:author xmlns:itunes=""', 'itunes:author')
                .replace('itunes:author xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"', 'itunes:author');

            await fs.writeFileAsync('podcast.xml', inp_xmls);
            res.json({message: `Podcast updated: http://dunwoodychurch.org/media?id=${req.podcastGuid}`, guid: `http://dunwoodychurch.org/media?id=${req.podcastGuid}`});
        } catch (err) {
            console.error(err);
            res.status(500).json(err);
        }
    });
}

exports.removePodcastEpisode = async function(req, res) {
    try {
        const guid = req.params.guid;

        const jsonObj = await parsePodcastXml(req, res);
        const previousImages = _.groupBy(jsonObj.rss.channel.item.filter(media => media['itunes:image'].href), media => media['itunes:image'].href.replace(/^.*[\\/]/, ''));
        const imageFilename = jsonObj.rss.channel.item.filter(media => media['itunes:image'].href).find(media => media.guid.replace('http://dunwoodychurch.org/media?id=', '') === guid)['itunes:image'].href.replace(/^.*[\\/]/, '')

        const file = await fs.readFileAsync('podcast.xml', 'utf8');
        const xmlDoc = new DOMParser().parseFromString(file, 'text/xml');

        let episodes = Array.from(xmlDoc.getElementsByTagName('item'));
        let selectedEpisode = episodes.find(episode => episode.getElementsByTagName('guid')[0].textContent.replace('http://dunwoodychurch.org/media?id=', '') == guid)

        selectedEpisode.parentNode.removeChild(selectedEpisode.nextSibling);
        selectedEpisode.parentNode.removeChild(selectedEpisode);

        var XMLS = new XMLSerializer();
        var inp_xmls = XMLS.serializeToString(xmlDoc)
            .replace('itunes:author xmlns:itunes=""', 'itunes:author')
            .replace('itunes:author xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"', 'itunes:author');

        await fs.writeFileAsync('podcast.xml', inp_xmls);

        if (previousImages[imageFilename].length <= 1) {
            const params = {
                Bucket: 'dccpodcasts',
                Delete: {
                    Objects: [
                        {Key: imageFilename}
                    ]
                }
                // other options supported by deleteObject, except Body and ContentLength.
                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObject-property
            };
            await new Promise((resolve, reject) => {
                const uploader = client.deleteObjects(params);
                uploader.on('error', function(err) {
                    console.error('unable to delete:', err.stack);
                    reject(err);
                });
                uploader.on('progress', function() {
                    sse.send({action: 'Deleting', file: imageFilename, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
                });
                uploader.on('end', async function() {
                    sse.send({action: 'Deleting', file: imageFilename, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                    resolve()
                });
            });
        }

        const params = {
            Bucket: 'dccpodcasts',
            Delete: {
                Objects: [
                    {Key: `${guid}.mp3`}
                ]
            }
            // other options supported by deleteObject, except Body and ContentLength.
            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObject-property
        };
        await new Promise((resolve, reject) => {
            const uploader = client.deleteObjects(params);
            uploader.on('error', function(err) {
                console.error('unable to delete:', err.stack);
                reject(err);
            });
            uploader.on('progress', function() {
                sse.send({action: 'Deleting', file: `${guid}.mp3`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: false}, 's3UploadStatus');
            });
            uploader.on('end', async function() {
                sse.send({action: 'Deleting', file: `${guid}.mp3`, percent: `${(uploader.progressAmount * 100 / uploader.progressTotal).toFixed(0)}%`, done: true}, 's3UploadStatus');
                resolve()
            });
        });

        res.json({message: `Podcast removed: http://dunwoodychurch.org/media?id=${guid}`, guid: `http://dunwoodychurch.org/media?id=${guid}`});
    } catch (err) {
        console.error(err);
        res.status(500).json(err);
    }
}

exports.setUuid = function (req, res, next) {
    req.podcastGuid = req.params.guid || uuidv4();
    next();
}

exports.getUser = async function(req, res) {
    res.json({user: req.user && req.user.username});
}

exports.authenticate = async function(req, res, next) {
    passport.authenticate('local', function (err, user) {
        if (err || !user) {
            return res.status(401).json({message: 'Invalid password.'});
        }

        req.login(user, loginErr => {
            if (loginErr) {
                return next(loginErr);
            }
            return res.json({user: req.user.username});
        });
    })(req, res, next);
}

exports.ensureLoggedIn = function(redirect = '/', req, res, next) {
    if (!req.user) {
        res.redirect(redirect)
    } else if (
        req.user._id == 1 && redirect != '/directory' ||
        req.user._id == 2 && redirect != '/teacherportal' ||
        req.user._id == 3 && redirect != '/podcastadmin'
    ) {
        req.logout();
        res.redirect(redirect);
    } else {
        next();
    }
}

exports.getCurriculumns = async function(req, res) {
    try {
        const data = await rp({
            uri: 'https://api.planningcenteronline.com/services/v2/media?filter=document&include=attachments',
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.requestAttachmentDownload = async function(req, res) {
    try {
        const data = await rp({
            method: 'POST',
            uri: `https://api.planningcenteronline.com/services/v2/media/${req.body.mediaId}/attachments/${req.body.attachmentId}/open`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getServicePlans = async function(req, res) {
    try {
        let data = await rp({
            uri: 'https://api.planningcenteronline.com/services/v2/service_types/139363/plans?order=sort_date&filter=future&per_page=4',
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);

        for (let plan of data.data) {
            let [teamMembers, attachments] = await Promise.all([
                rp({
                    uri: `https://api.planningcenteronline.com/services/v2/service_types/139363/plans/${plan.id}/team_members?per_page=100`,
                    json: true
                }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET),
                rp({
                    uri: `https://api.planningcenteronline.com/services/v2/service_types/139363/plans/${plan.id}/attachments?per_page=100`,
                    json: true
                }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)
            ]);
            teamMembers.data = teamMembers.data.filter(teamMember => teamMember.relationships.team.data.id == '472548');
            plan.team_members = teamMembers;
            plan.attachments = attachments;
        }

        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.requestPlanAttachmentDownload = async function(req, res) {
    try {
        const data = await rp({
            method: 'POST',
            uri: `https://api.planningcenteronline.com/services/v2/service_types/139363/plans/${req.body.planId}/attachments/${req.body.attachmentId}/open`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getTeams = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/services/v2/teams/3017627?include=person_team_position_assignments,people,team_positions`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getPerson = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/people/v2/people/${req.params.personId}?include=addresses,emails,phone_numbers`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getPeople = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/people/v2/lists/431741?include=people`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getHouseholdLeaders = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/people/v2/lists/431739?include=people`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getHouseholds = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/people/v2/households?per_page=100&include=people`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        for (let offset = 100; data.meta.total_count > data.data.length; offset += 100) {
            let moreData = await rp({
                uri: `https://api.planningcenteronline.com/people/v2/households?per_page=100&offset=100&include=people`,
                json: true
            }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
            data.data = [...data.data, ...moreData.data];
            data.meta.count = data.data.length;
        }
        data.included = [];
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getBlog = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://dunwoodyvoices.org/rss`,
        });
        res.json(fastXmlParser.parse(data));
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getWorshipSamples = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/services/v2/media/2241204/attachments`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);
        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getChurchEvents = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/resources/v2/resources/323527/resource_bookings?order=starts_at&include=event&filter=future&where[ends_at][lte]=${moment().add(1, 'month').format('YYYY-MM-DD')}&per_page=100`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);

        for (let event of data.included.filter(include => include.type == 'Event')) {
            if (event.relationships.attachments && event.relationships.attachments.data.length == 0) {
                continue;
            }

            let attachments = await rp({
                uri: `https://api.planningcenteronline.com/resources/v2/events/${event.id}/attachments`,
                json: true
            }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)

            data.included.find(include => include.id == event.id).relationships.attachments = attachments;
        }

        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getSpecialEvents = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/resources/v2/resources/323902/resource_bookings?order=starts_at&include=event&filter=future`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);

        for (let event of data.included.filter(include => include.type == 'Event')) {
            if (event.relationships.attachments && event.relationships.attachments.data.length == 0) {
                continue;
            }

            let attachments = await rp({
                uri: `https://api.planningcenteronline.com/resources/v2/events/${event.id}/attachments`,
                json: true
            }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)

            data.included.find(include => include.id == event.id).relationships.attachments = attachments;
        }

        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.getYouthEvents = async function(req, res) {
    try {
        const data = await rp({
            uri: `https://api.planningcenteronline.com/resources/v2/resources/323912/resource_bookings?order=starts_at&include=event&filter=future&where[ends_at][lte]=${moment().add(1, 'month').format('YYYY-MM-DD')}&per_page=100`,
            json: true
        }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET);

        for (let event of data.included.filter(include => include.type == 'Event')) {
            if (event.relationships.attachments && event.relationships.attachments.data.length == 0) {
                continue;
            }

            let attachments = await rp({
                uri: `https://api.planningcenteronline.com/resources/v2/events/${event.id}/attachments`,
                json: true
            }).auth(process.env.PLANNING_CENTER_APPLICATION_ID, process.env.PLANNING_CENTER_SECRET)

            data.included.find(include => include.id == event.id).relationships.attachments = attachments;
        }

        res.json(data);
    } catch (err) {
        res.status(501).json(err);
    }
}

exports.sendS3Updates = sse.init;

exports.errorHandler = function(err, req, res, next) {
    res.status(500).send({message: err.message});
    next();
}