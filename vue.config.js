module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:51111',
                ws: true,
                changeOrigin: true
            },
        }
    }
}