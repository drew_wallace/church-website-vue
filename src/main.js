import Vue from "vue";
import VueRouter from "vue-router";
import Toasted from 'vue-toasted';

import App from "./App.vue";
import Home from "./components/Home.vue";
import GetInvolved from "./components/GetInvolved.vue";
import Give from "./components/Give.vue";
import Gifts from "./components/Gifts.vue";
import Youth from "./components/Youth.vue";
import About from "./components/About.vue";
import Events from "./components/Events.vue";
import Media from "./components/Media.vue";
import TeacherPortal from "./components/TeacherPortal.vue";
import Curriculum from "./components/Curriculum.vue";
import Teams from "./components/Teams.vue";
import Directory from "./components/Directory.vue";
import PodcastAdmin from "./components/PodcastAdmin.vue";


Vue.config.productionTip = false;
Vue.use(Toasted, {position: 'top-center', containerClass: 'toast-top-fix', duration: 5000})
Vue.use(VueRouter);

export const globalState = {
  loggedIn: false
}

const routes = [
  { path: '/', component: Home },
  { path: '/getinvolved', component: GetInvolved },
  { path: '/give', component: Give },
  { path: '/gifts', component: Gifts },
  { path: '/youth', component: Youth },
  { path: '/about', component: About },
  { path: '/events', component: Events },
  { path: '/teacherportal', component: TeacherPortal },
  { path: '/teacherportal/curriculum/:curriculumId', component: Curriculum },
  { path: '/teacherportal/teams', component: Teams },
  { path: '/directory', component: Directory },
  { path: '/media', component: Media },
  { path: '/podcastadmin', component: PodcastAdmin }
];

const router = new VueRouter({
  mode: "history",
  routes // short for `routes: routes`
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
